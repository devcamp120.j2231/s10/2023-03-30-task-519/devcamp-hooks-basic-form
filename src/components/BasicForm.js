import { useEffect, useState } from "react";

const BasicForm = () => {
    const [firstname, setFirstname] = useState(localStorage.getItem("firstname") || "");
    const [lastname, setLastname] = useState(localStorage.getItem("lastname") || "");

    const firstnameInputChangeHandler = (event) => {
        setFirstname(event.target.value);
    }

    const lastnameInputChangeHandler = (event) => {
        setLastname(event.target.value);
    }

    useEffect(() => {
        localStorage.setItem("firstname", firstname);
        localStorage.setItem("lastname", lastname);
    }, [firstname, lastname])

    return (
        <div>
            <input value={firstname} onChange={firstnameInputChangeHandler} />
            <br></br>
            <input value={lastname} onChange={lastnameInputChangeHandler}/>
            <p>{firstname} {lastname}</p>
        </div>
    )
}

export default BasicForm;